<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous" />
  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css" />

  <!-- Bootstrap Icons -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css" />

  <!-- My CSS -->
  <link rel="stylesheet" href="src/style.css" />

  <title>Semantic Web</title>
</head>

<body id="home">
  <!-- Navbar -->
  <!--PHP-->
  <?php
  require_once("sparqllib.php");
  $test = "";
  if (isset($_POST['search-university'])) {
    $test = $_POST['search-university'];
    $data = sparql_get(
      "http://localhost:3030/caub",
    "
    PREFIX caub:<http://www.semanticweb.org/xafixx/ontologies/2023/6/caub_19102136#>

SELECT ?nama_wisatawan ?alamat ?jenis_kelamin ?nama_peralatan ?harga_sewa ?nomor_tiket ?harga_tiket ?link
WHERE{
	?wisatawan caub:nama_wisatawan ?nama_wisatawan;
           	   caub:alamat ?alamat;
               caub:jenis_kelamin ?jenis_kelamin;
               caub:menyewa ?peralatan;
               caub:membeli ?tiket;
  			   caub:mengunjungi ?jejaring.
  	?peralatan caub:nama_peralatan ?nama_peralatan;
               caub:harga_sewa ?harga_sewa.
  	?tiket 	   caub:nomor_tiket ?nomor_tiket;
               caub:harga_tiket ?harga_tiket.
  	?jejaring  caub:link ?link.
    FILTER (regex (?nama_wisatawan,  '$test', 'i') || regex (?nomor_tiket,  '$test', 'i'))
  }
    "
    );
  } else {
    $data = sparql_get(
    "http://localhost:3030/caub",
    
    "
    PREFIX caub:<http://www.semanticweb.org/xafixx/ontologies/2023/6/caub_19102136#>

SELECT ?nama_wisatawan ?alamat ?jenis_kelamin ?nama_peralatan ?harga_sewa ?nomor_tiket ?harga_tiket ?link
WHERE{
	?wisatawan caub:nama_wisatawan ?nama_wisatawan;
           	   caub:alamat ?alamat;
               caub:jenis_kelamin ?jenis_kelamin;
               caub:menyewa ?peralatan;
               caub:membeli ?tiket;
  			   caub:mengunjungi ?jejaring.
  	?peralatan caub:nama_peralatan ?nama_peralatan;
               caub:harga_sewa ?harga_sewa.
  	?tiket 	   caub:nomor_tiket ?nomor_tiket;
               caub:harga_tiket ?harga_tiket.
  	?jejaring  caub:link ?link.
  }
    "
    );
  }

  if (!isset($data)) {
    print "<p>Error: " . sparql_errno() . ": " . sparql_error() . "</p>";
  }

  ?>
  <!-- Akhir Navbar -->

  <!-- About -->
  <section id="about">
    <div class="container">

    <div class="row text-center mb-3 mt-0 hasil">
        <div class="col">
          <h2>Cari Data Wisatawan Di Sini!</h2>
        </div>
      </div>
      <div class="row tentang">
        <div class="text-center mt-5 ">
          <form action="" method="post" id="nameform">
            <div class="search-box">
              <input type="text" name="search-university" placeholder="Cari Tim" />
              <button type="submit" class="btn btn-primary">Cari</button>
            </div>
            <i class="bi bi-search"></i>
        </div>
        </form>
      </div>

      <!-- Hasil Pencarian -->
      <div class="row fs-5">
        <div class="col-md-5">
          <p>
            Menampilkan Pencarian :
            <br />
          </p>
          <p>
            <span>
              <?php
              if ($test != NULL) {
                echo $test;
              } else {
                echo "Hasil Pencarian Kamu :";
              }
              ?></span>
          </p>
        </div>
      </div>

      <div class="row">

        <?php $i = 0; ?>
        <?php foreach ($data as $dat) : ?>
          <div class="col-md-4">
            <div class="box">
              <ul class="list-group list-group-flush">
                
                <div class="header-data"> <b>Nama Wisatawan :</b></div>
                <div class="item-data"><?= $dat['nama_wisatawan'] ?></div>

                <div class="header-data"> <b>Alamat :</b></div>
                <div class="item-data"><?= $dat['alamat'] ?></div>

                <div class="header-data"> <b>Jenis Kelamin :</b></div>
                <div class="item-data"><?= $dat['jenis_kelamin'] ?></div>

                <div class="header-data"> <b>Peralatan Yang Disewa :</b></div>
                <div class="item-data"><?= $dat['nama_peralatan'] ?></div>
                
                <div class="header-data"> <b>Harga Sewa Peralatan :</b></div>
                <div class="item-data"><?= $dat['harga_sewa'] ?></div>
                
                <div class="header-data"> <b>Nomor Tiket :</b></div>
                <div class="item-data"><?= $dat['nomor_tiket'] ?></div>
               
                <div class="header-data"> <b>Harga Tiket :</b></div>
                <div class="item-data"><?= $dat['harga_tiket'] ?></div>
                
                <div class="header-data"> <b>Wisatawan mengunjungi jejaring sosial :</b></div>
                <div class="item-data"><?= $dat['link'] ?></div>


              </ul>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </section>
  <!-- Akhir About -->

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>

</html>