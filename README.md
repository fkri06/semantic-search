# Semantic Web Project

<p>Repository ini menyimpan project Model Ontology untuk
UAS Mata Kuliah Semantic Web. Untuk project ini, tempat wisata
yang menjadi studi kasus untuk model ontology nya adalah
Wisata Camp Area Umbul Bengkok (CAUB) yang berlokasi di Dusun II, Desa Karangsalam, Kecamatan Baturraden, Kabupaten Banyumas, Jawa Tengah.
</p>

<p>
Filter pada program web php menggunakan dua variable yaitu nama wisatawan dan nomor tiket.
</p>

# HALAMAN WEB

![Alt text](screenshots/screencapture-localhost-2023-07-18-09_05_43.png?)

# PECARIAN NAMA

![Alt text](screenshots/screencapture-localhost-2023-07-18-09_08_47.png?)
![Alt text](screenshots/screencapture-localhost-2023-07-18-09_09_09.png?)
![Alt text](screenshots/screencapture-localhost-2023-07-18-09_09_27.png?)

# PENCARIAN NOMOR TIKET

![Alt text](screenshots/tiket0001.png?)
![Alt text](screenshots/tiket0002.png?)
![Alt text](screenshots/tiket0003.png?)
